﻿(function () {

    angular.module('tgsApp')
        .controller('tgsController', tgsController);

    tgsController.$inject = [
        '$scope',
        '$http'
    ];

    function tgsController($scope, $http) {

        var vm = $scope;
        vm.clic = function(){
            var algo = 1;
        };

        /// params
        vm.vm_nodes = [];
        vm.propConstant = [];

        activate();
        vm.reset = _reset;

        // methods
        vm.setAccumField = _setAccumField;

        // functions
        function activate(){
            console.log("Activate");
            vm.vm_nodes = nodes;
            _setPropConstant();
            _setAccumsValue();

            setNodesValue();
            DrawNodes();
            DrawLinks();
        }

        function _reset(){
            vm.vm_nodes = nodes;
            _setPropConstant();
            _setAccumsValue();

            setNodesValue();
            DrawNodes();
        }

        function _setAccumsValue() {
            vm.vm_nodes[0].size = 30; //Adaptación al Entorno
            vm.vm_nodes[1].size = 48; //Cobertura Académica
            vm.vm_nodes[2].size = 16; //Corrupción
            vm.vm_nodes[3].size = 23; //Costo Manutención
            vm.vm_nodes[4].size = 8; //Costo Transporte
            vm.vm_nodes[5].size = 14; //Costo Período Académico
            vm.vm_nodes[6].size = 60; //Desarrollo Estructura Vial
            vm.vm_nodes[7].size = 28; //Deserción
            vm.vm_nodes[8].size = 37; //Disciplina Estudiante
            vm.vm_nodes[9].size = 35; //Escasez Recursos Económicos
            vm.vm_nodes[10].size = 56; //Estabilidad Entorno Familiar
            vm.vm_nodes[11].size = 27; //Facilidad Movilidad
            vm.vm_nodes[12].size = 25; //Inseguridad
            vm.vm_nodes[13].size = 38; //Instituciones
            vm.vm_nodes[14].size = 57; //Inversión Estatal
            vm.vm_nodes[15].size = 32; //Interés Contenido Académico
            vm.vm_nodes[16].size = 47; //Oportunidades Laborales
            vm.vm_nodes[17].size = 36; // Orientación Profesional
            vm.vm_nodes[18].size = 23; //Principios y Motivación
            vm.vm_nodes[19].size = 40; //Rendimiento Estudiante
            vm.vm_nodes[20].size = 60; //Resultados Evaluación
            vm.vm_nodes[21].size = 15; //Subsidios
        }

        function _setPropConstant() {
            // v1 → Variable 1
            // v2 → Variable 2
            // p → Polarity
            // pC → Proportionality

            vm.propConstant = [
                { v1: 'AE', v2: 'D', add: false, pC: 12 },   // Adaptación Entorno - Deserción
                { v1: 'CA', v2: 'OL', add: true, pC: 35 },   // Cobertura Académica - Oportunidades Laborales
                { v1: 'CA', v2: 'CPA', add: false, pC: 28 },  // Cobertura Académica - Costo Perd Académico
                { v1: 'CA', v2: 'OP', add: true, pC: 50 },    // Cobertura Académica - Oportunidad laboral
                { v1: 'C', v2: 'IE', add: true, pC: 70 },    // Corrupción - Inversión Estatal
                { v1: 'CM', v2: 'ERE', add: false, pC: 25 },   // Costo Manutención - Escasez Recursos Económicos
                { v1: 'CT', v2: 'ERE', add: false, pC: 8 },  // Costo Transporte - Escasez Recursos Económicos
                { v1: 'CPA', v2: 'ERE', add: false, pC: 50 }, // Costo Prdo Acad. - Escasez Recursos Económicos
                { v1: 'DEV', v2: 'FM', add: true, pC: 48 },  // Dllo Estr. Vial - Facilidad Movilidad
                { v1: 'D', v2: 'C', add: false, pC: 60 },     // Deserción - Corrupción
                { v1: 'D', v2: 'OP', add: true, pC: 36 },    // Deserción - Orientación Profesional
                { v1: 'D', v2: 'I', add: false, pC: 10 },     // Deserción - Inseguridad
                { v1: 'DE', v2: 'D', add: false, pC: 10 },    // Disciplina Estudiante - Deserción
                { v1: 'ERE', v2: 'D', add: false, pC: 80 },   // Escasez Recursos Económicos - Deserción
                { v1: 'ERE', v2: 'EEF', add: true, pC: 25 }, // Escasez Recursos Económicos - Estabilidad Ent Fam
                { v1: 'EEF', v2: 'D', add: false, pC: 12 },    // Estabilidad Ent Fam - Deserción
                { v1: 'FM', v2: 'CT', add: false, pC: 10 },   // Facilidad Movilidad - Costo Transporte
                { v1: 'I', v2: 'FM', add: true, pC: 14 },     // Inseguridad - Facilidad Movilidad
                { v1: 'IT', v2: 'CA', add: true, pC: 75 },   // Instituciones - Cobertura Académica
                { v1: 'IE', v2: 'DEV', add: true, pC: 45 },  // Inversión Estatal - Dll Estr. Vial
                { v1: 'IE', v2: 'S', add: true, pC: 25 },     // Inversión Estatal - Subsidios
                { v1: 'IE', v2: 'IT', add: true, pC: 35 },   // Inversión Estatal -Instituciones
                { v1: 'IE', v2: 'I', add: false, pC: 50 },    // Inversión Estatal -Inseguridad
                { v1: 'ICA', v2: 'D', add: false, pC: 27 },   // Interés Cont. Acad. - Deserción
                { v1: 'OL', v2: 'CT', add: false, pC: 18 },    // Oportunidades Laborales - Costo Transporte
                { v1: 'OL', v2: 'CM', add: false, pC: 28 },    // Oportunidades Laborales - Costo Manutención
                { v1: 'OL', v2: 'CPA', add: false, pC: 30 },  // Oportunidades Laborales - Costo Pdo Acad.
                { v1: 'OP', v2: 'PM', add: true, pC: 56 },   // Orientación Profesional - Principios y Motivación
                { v1: 'OP', v2: 'DE', add: true, pC: 38 },   // Orientación Profesional - Disciplina Estudiante
                { v1: 'OP', v2: 'ICA', add: true, pC: 65 },  // Orientación Profesional - Interés Cont. Acad.
                { v1: 'OP', v2: 'AE', add: true, pC: 9 },   // Orientación Profesional - Adaptación al Entorno
                { v1: 'PM', v2: 'RE', add: true, pC: 62 },   // Principios y Motivación - Rendimiento Estudiante
                { v1: 'RE', v2: 'REV', add: true, pC: 75 },  // Rendimiento Estudiante - Resultados Evaluación
                { v1: 'REV', v2: 'D', add: false, pC: 45 },   // Resultados Evaluación - Deserción
                { v1: 'S', v2: 'CPA', add: false, pC: 30 },   // Subsidios - Costo Pdo Académico
                { v1: 'S', v2: 'CT', add: false, pC: 15 }     // Subsidios - Costo Transporte
            ];
        }

        function _setAccumField(index) {
            var modifiedNode = vm.vm_nodes[index];
            var relations = getRelationsByModNode(modifiedNode);

            // Apply math operation according to each proportionality const.

            var lengthRel = relations.length;
            for (var i = 0; i < lengthRel; i++) {
                var propConst = relations[i].propConst;
                
                var nodeV1 = Enumerable.From(vm.vm_nodes).Where(function (x) { return x.acronym == propConst.v1 ; }).ToArray()[0];
                var nodeV2 = Enumerable.From(vm.vm_nodes).Where(function (x) { return x.acronym == propConst.v2 ; }).ToArray()[0];

                var changeValue = Math.round(modifiedNode.size * propConst.pC) / 100;
                //var newSize = /*nodeV2.size*/ 0 + (propConst.add == true ? -changeValue : changeValue);
                // var newSize = changeValue;
                var newSize = (propConst.add == true ? 100-changeValue : changeValue);
                if (newSize < 0.01)
                    newSize = 0.01;
                vm.vm_nodes[nodeV2.index].size = Math.round(newSize * 100) / 100;
            };

            setNodesValue();

            DrawNodes();
        }

        //Get relations by modified node.
        function getRelationsByModNode(modifiedNode) {
            // First round
            var relations = getRelatedByNode([], modifiedNode);
            var lengthRel = relations.length;

            // Second round
            cont = 0;
            while (lengthRel < vm.vm_nodes.length - 1){
                relations = getRelatedByNode(relations, relations[cont], modifiedNode.acronym);
                lengthRel = relations.length;
                cont ++;
            }
            return relations;
        }

        function getRelatedByNode(relations, node, acronymModifiedNode) {
            var length = node.connectsTo.length;
            for (var i = 0; i < length; i++) {
                var relatedNode = Enumerable.From(vm.vm_nodes).Where(function (x) { return x.acronym == node.connectsTo[i]; }).ToArray();
                if (relatedNode.length > 0) {
                    relatedNode = relatedNode[0];
                    var propConst = Enumerable.From(vm.propConstant).Where(function (x) { return x.v1 == node.acronym && x.v2 == relatedNode.acronym; }).ToArray()[0];

                    relatedNode.propConst = propConst;
                    relations = addRelatedNode(relations, relatedNode, acronymModifiedNode);
                    if (relations.length >= vm.vm_nodes.length)
                        return relations;
                }
            }
            return relations;
        }

        function addRelatedNode(relations, relatedNode, acronymModifiedNode){
            if (relatedNode.acronym !== acronymModifiedNode) {
                var array = Enumerable.From(relations).Where(function (x) { return x.acronym == relatedNode.acronym; }).ToArray();
                if (array.length === 0) {
                    var myAcr = relatedNode.acronym;
                    relations.push(relatedNode);
                }
            }
            return relations;
        }

        function setNodesValue() {
            nodes = vm.vm_nodes;
        }
    }



})();