// var nodes = [
//    {name: 'A', row: 2, column: 2, connectsTo: ['D','F'], size: 0, color: "blue", polarity: true, acronym: "A"},
//    {name: 'B', row: 2, column: 5, connectsTo: ['D'], size: 0, color: "blue", polarity: false, acronym: "B"},
//    {name: 'C', row: 2, column: 8, connectsTo: ['D'], size: 0, color: "blue", polarity: true, acronym: "C"},
//    {name: 'D', row: 5, column: 5, connectsTo: ['F'], size: 0, color: "blue", polarity: true, acronym: "D"},
//    {name: 'E', row: 8, column: 2, connectsTo: ['F'], size: 0, color: "blue", polarity: true, acronym: "E"},
//    {name: 'F', row: 8, column: 5, size: 0, color: "blue", polarity: true, acronym: "F"}
// ];

var nodes = [
    { acronym: 'AE',  row: 1, column: 8, size: 1, name: 'Adaptación al Entorno',        connectsTo: ['D'], index: 0, polarity:[false]},
    { acronym: 'CA',  row: 6, column: 12, size: 1, name: 'Cobertura Académica',        connectsTo: ['OL', 'CPA', 'OP'], index: 1, polarity:[true, false, true]},
    { acronym: 'C',   row: 4, column: 9, size: 1, name: 'Corrupción',                   connectsTo: ['IE'], index: 2, polarity:[true]},
    { acronym: 'CM',  row: 14, column: 15, size: 1, name: 'Costo Manutención',          connectsTo: ['ERE'], index: 3, polarity:[false]},
    { acronym: 'CT',  row: 9, column: 15, size: 1, name: 'Costo Transporte',           connectsTo: ['ERE'], index: 4, polarity:[false]},
    { acronym: 'CPA', row: 12, column: 10, size: 1, name: 'Costo Período Académico',    connectsTo: ['ERE'], index: 5, polarity:[false]},
    { acronym: 'DEV', row: 4, column: 15, size: 1, name: 'Desarrollo Estructura Vial',  connectsTo: ['FM'], index: 6, polarity:[true]},
    { acronym: 'D',   row: 8, column: 7, size: 1, name: 'Deserción',                    connectsTo: ['C', 'OP', 'I'], index: 7, polarity:[false, true, false], color:"#4285f4"},
    { acronym: 'DE',  row: 3, column: 6, size: 1, name: 'Disciplina Estudiante',        connectsTo: ['D'], index: 8, polarity:[false]},
    { acronym: 'ERE', row: 14, column: 10, size: 1, name: 'Escasez Recursos Económicos',connectsTo: ['D', 'EEF'], index: 9, polarity:[false, true]},
    { acronym: 'EEF',row: 14, column: 4 , size: 1, name: 'Estabilidad Entorno Familiar',connectsTo: ['D'], index: 10, polarity:[false]},
    { acronym: 'FM', row: 7, column: 15, size: 1, name: 'Facilidad Movilidad',          connectsTo: ['CT'], index: 11, polarity:[false]},
    { acronym: 'I',  row: 7, column: 9, size: 1, name: 'Inseguridad',                 connectsTo: ['FM'], index: 12, polarity:[true]},
    { acronym: 'IT', row: 4, column: 12, size: 1, name: 'Instituciones',                 connectsTo: ['CA'], index: 13, polarity:[true]},
    { acronym: 'IE', row: 1, column: 12, size: 1, name: 'Inversión Estatal',            connectsTo: ['DEV', 'S', 'IT', 'I'], index: 14, polarity:[true, true, true, false]},
    { acronym: 'ICA',row: 8, column: 3, size: 1, name: 'Interés Contenido Académico',  connectsTo: ['D'], index: 15, polarity:[false]},
    { acronym: 'OL', row: 12, column: 15, size: 1, name: 'Oportunidades Laborales',      connectsTo: ['CT', 'CM', 'CPA'], index: 16, polarity:[false, false, false]},
    { acronym: 'OP', row: 3, column: 3, size: 1, name: 'Orientación Profesional',       connectsTo: ['PM', 'DE', 'ICA', 'AE'], index: 17, polarity:[true, true, true, true]},
    { acronym: 'PM', row: 5, column: 1, size: 1, name: 'Principios y Motivación',     connectsTo: ['RE'], index: 18, polarity:[true]},
    { acronym: 'RE', row: 9, column: 1, size: 1, name: 'Rendimiento Estudiante',      connectsTo: ['REV'], index: 19, polarity:[true]},
    { acronym: 'REV',row: 12, column: 1, size: 1, name: 'Resultados Evaluación',        connectsTo: ['D'], index: 20, polarity:[false]},
    { acronym: 'S',  row: 9, column: 10, size: 1, name: 'Subsidios',                    connectsTo: ['CPA', 'CT'], index: 21,  polarity:[false, false]},
];

var diagram = new SimpleDiagram('#diagram', {
    addGrid: true,
    cellSize: 45,
    numColumns: 15,
    numRows: 14,
    interactive: true
});

// Draw the nodes!
function DrawNodes(){
    nodes.forEach(function(node) {
        diagram.addNode({
            name: node.acronym,
            label: node.acronym+"("+node.size+")",
            row: node.row,
            column: node.column,
            //hoverText:  node.name + "("+node.row+","+node.column+")",
            hoverText:  node.name + " (" + node.size + " %)",
            shapeStyle: node.color|| "#C5C5C5"
        });
    });
}

function DrawLinks(){
    // Draw the links!    
    nodes.forEach(function(node) {
        if (!node.connectsTo)
            return;

        diagram.addLine({
            from: node.acronym,
            to: node.connectsTo,
            addArrow: true,                
            polarity: node.polarity
        });
    });
}

function dodo(){
    diagram = new SimpleDiagram('#diagram', {
        addGrid: true,
        cellSize: 45,
        numColumns: 15,
        numRows: 14,
        interactive: true
        });
    DrawNodes();
    DrawLinks();
}

//DrawNodes();
//DrawLinks();



// mio
// nodes[0].acronym = "T";
// DrawNodes();
// nodes[0].acronym = "O";
// DrawNodes();


// function get_node(source){
//     return json.nodes.filter(function(obj){
//         console.log(obj.source);
//         return obj.source == source;    
//     })[0];
// }


// function DoShit(){
//     changeNodeSize(get_node(1).source,0);   
//     changeNodeSize(1,200);
//     changeNodeSize(1,300);    
//     changeNodeSize(get_node(4).source,400);    
// }


$( "#description" ).on( "click2", function() {
  alert( $( this ).text() );
});


$( "#description" ).on( "custom", function( event) {
  alert( "param1 param2" );
});

function changeDescription(obj){
     //console.log(obj);
    $("#description").text( obj.hoverText);
    // $("#description").text(obj.hoverText + " "+ obj.label);
}

function flushDescription(){
    $("#description").text(null);
}